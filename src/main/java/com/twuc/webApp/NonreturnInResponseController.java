package com.twuc.webApp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NonreturnInResponseController {

    //2.1
    @GetMapping("/api/no-return-value")
    public void getNoReturn() {
    }

}
