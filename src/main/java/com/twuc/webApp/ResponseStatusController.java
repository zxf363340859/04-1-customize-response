package com.twuc.webApp;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ResponseStatusController {

    @GetMapping("/api/no-return-value-with-annotation ")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void getResponseStatus() {

    }

}
