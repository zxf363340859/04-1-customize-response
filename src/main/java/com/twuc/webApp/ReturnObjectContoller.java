package com.twuc.webApp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ReturnObjectContoller {

    @GetMapping("/api/message-objects/{message}")
    public Cat getCat(@PathVariable String message) {
        return new Cat(message);
    }

}
