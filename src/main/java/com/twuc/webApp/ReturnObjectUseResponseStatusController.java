package com.twuc.webApp;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ReturnObjectUseResponseStatusController {

    @GetMapping("/api/message-objects-with-annotation/{message}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Cat getCat(@PathVariable String message) {
        return new Cat(message);
    }

}
