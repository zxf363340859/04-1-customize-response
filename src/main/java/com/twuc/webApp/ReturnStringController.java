package com.twuc.webApp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ReturnStringController {

    @GetMapping("/api/messages/{message}")
    public String returnString(@PathVariable String message) {
        return message;
    }

}
