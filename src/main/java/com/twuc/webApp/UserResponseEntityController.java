package com.twuc.webApp;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserResponseEntityController {

    @GetMapping("/api/message-entities/{message}")
    public ResponseEntity<Cat> getCat(@PathVariable String message) {
       return ResponseEntity.status(200).header("X-Auth", "me").body(new Cat(message));
    }


}
