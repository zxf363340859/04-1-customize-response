package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class NonReturnInResponseControllerTest {

    @Autowired
    private MockMvc mockMvc;

    //2.1
    @Test
    void test_no_return_response_controller() throws Exception {
        mockMvc.perform(get("/api/no-return-value")).andExpect(status().is(200));
    }
}
